import logo from './logo.svg';
import './App.css';
import { useDispatch, useSelector } from 'react-redux';
import { ActiveAction } from './state/action/exampleAction';

function App() {
  const dispatch = useDispatch()
  const exReducer =  useSelector(({exampleReducer})=> exampleReducer)
  console.log(exReducer)

  const handleClick = () => {
    ActiveAction(!exReducer.isActive)(dispatch)
  }

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>Status: {exReducer.isActive.toString()}</p>
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <button style={{padding: 10}} onClick={handleClick}>
          change status
        </button>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
